import datetime as dt
import os
import sys

from instapostautomatized import ALREADY_PLANNED, create_serie_of_posts

DIR_PHOTOS_SORTED = sys.argv[1]
print(DIR_PHOTOS_SORTED)
if not os.path.isdir(DIR_PHOTOS_SORTED):
    raise ValueError(
        f"Invalid path argument. Path must be a directory. Current value: {DIR_PHOTOS_SORTED}"
    )

DIRS = []
for dirpath, _, _ in os.walk(DIR_PHOTOS_SORTED, topdown=True):
    if dirpath != DIR_PHOTOS_SORTED and ALREADY_PLANNED not in os.listdir(dirpath):
        DIRS.append(dirpath)
# print(DIRS)

DATE_FIRST_POST = dt.datetime.strptime(sys.argv[2], "%d-%m-%Y")
DATE_FIRST_POST += dt.timedelta(hours=7)
# print(DATE_FIRST_POST)


last_post_date = DATE_FIRST_POST
for idx, dir in enumerate(DIRS):
    print(idx, dir)
    # use this for testing to limit damages if bug occurs
    if idx >= 1:
        break

    create_serie_of_posts(dir, last_post_date)
    input()

    # mark folder as already done instead of deleting, bcz meta business is shit
    # and I lost a few planned posts and can't find the photos and their combination again...
    # with open(os.path.join(dir, ALREADY_PLANNED), "w") as f:
    #     f.write("already planned")
    #     print(f"Directory {dir} fully planned!")
    # last_post_date += dt.timedelta(days=1)
