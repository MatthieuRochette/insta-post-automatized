from time import sleep

from .window import win


def strf_keys(keys_to_send: str) -> str:
    return (
        keys_to_send.replace(" ", "{SPACE}")
        .replace("\n", "{ENTER}")
        .replace("\t", "{TAB}")
    )


def send_keys(keys, n: int = 1, win=win):
    formatted = strf_keys(keys)
    for _ in range(n):
        print(formatted)
        win.type_keys(formatted)
        sleep(0.4)


def tab(n: int = 1, win=win):
    send_keys(keys="\t", n=n, win=win)


def s_tab(n: int = 1, win=win):
    send_keys(keys="+\t", n=n, win=win)


def space(n: int = 1, win=win):
    send_keys(keys=" ", n=n, win=win)


def enter(n: int = 1, win=win):
    send_keys(keys="\n", n=n, win=win)
