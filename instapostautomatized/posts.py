import os
from .window import Desktop
from time import sleep
import datetime as dt
from .utils import enter, send_keys, s_tab, space, tab


def create_post(photo_dir_path: str, index_in_serie: int, date: dt.datetime):
    # Create post button
    tab(2)
    enter()
    sleep(5)

    # Open Photo select
    tab(3)
    enter(2)
    sleep(2.5)
    win_photo_select = Desktop().window(title_re=r"File Upload")

    # Select photo
    s_tab(win=win_photo_select)  # Go to photo view
    send_keys(
        "{RIGHT}{LEFT}", win=win_photo_select
    )  # Make all 3 photos selectable with enter
    send_keys("{RIGHT}", n=index_in_serie, win=win_photo_select)
    enter(win=win_photo_select)
    del win_photo_select

    # Enter post description
    tab()
    textfile_path = os.path.join(photo_dir_path, "zzz.txt")
    if os.path.isfile(textfile_path):
        with open(textfile_path) as text_file:
            text_lines = text_file.readlines()
            if len(text_lines) == 1:
                send_keys(text_lines[0])
            elif len(text_lines) >= index_in_serie + 1:
                if text_lines[index_in_serie].strip() in ["", "\n", "\r\n"]:
                    pass
                else:
                    # Use appropriate line if it exists and not empty
                    send_keys(text_lines[index_in_serie])
            sleep(3)
            space()
            send_keys("{RIGHT}")
    sleep(1)

    # Enter post location
    tab(2)
    locationfile_path = os.path.join(photo_dir_path, "zzz_location.txt")
    if os.path.isfile(locationfile_path):
        enter()
        sleep(0.75)
        tab()
        canceled = False
        with open(locationfile_path) as locationfile:
            lines = locationfile.readlines()
            if len(lines) == 1:
                # 1 line means same location for all photos
                send_keys(lines[0])
            elif len(lines) >= index_in_serie + 1:
                if lines[index_in_serie] in ["", "\n", "\r\n", " ", " \n", " \r\n"]:
                    s_tab()
                    enter()
                    canceled = True
                else:
                    # Use appropriate line if it exists and not empty
                    send_keys(lines[index_in_serie])
            else:
                # Cancel if line doesn't exist
                s_tab()
                enter()
                canceled = True

        if not canceled:
            sleep(1.8)
            enter()
            tab(2)
            enter()
            sleep(1)
            tab(2)
            sleep(1)
    else:
        sleep(1)

    # Program date and time of publication
    tab()
    space()
    tab()
    send_keys("^a")
    send_keys(date.strftime("%d/%m/%Y"))
    enter()
    time = date + dt.timedelta(minutes=index_in_serie)
    tab()
    send_keys(date.strftime("%H"))
    tab()
    send_keys(time.strftime("%M"))
    tab()
    send_keys("A")
    sleep(1)

    # Click confirm button
    tab(5)
    enter()
    sleep(3)


def create_serie_of_posts(photo_dir_path: str, date: dt.datetime):
    for i in range(3):
        print(f"i={i}")
        if i >= 1:
            break
        create_post(photo_dir_path, i, date)
        sleep(10)
