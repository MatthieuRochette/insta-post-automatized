from time import sleep

from pywinauto import WindowSpecification
from pywinauto.application import Application
from pywinauto import Desktop

app = Application(backend="uia").start(
    r"C:\Program Files\Mozilla Firefox\firefox.exe https://business.facebook.com/latest/content_calendar"
)

sleep(10)

win: WindowSpecification = Desktop(backend="uia").window(
    title_re=r"Meta.*Mozilla Firefox"
)
# print(windows.window_text)
# print([w.window_text() for w in windows])
# win: WindowSpecification = app.window(title_re=r".*Mozilla Firefox")
# print(win)
